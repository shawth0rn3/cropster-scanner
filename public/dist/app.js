//https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent

import { WEIGHT_UNIT_OPTIONS } from "./util/enums.js"
import { CERTIFICATES } from "./util/fixtures.js"

class App {
	constructor() {
		console.log(WEIGHT_UNIT_OPTIONS, CERTIFICATES);
	};

	attachEventListener(element, eventType, callback) {
		if (!element) return false;
		element.addEventListener(eventType, callback, false);
	};

	_calculateCanvasDimensions(uploadedImage) {
		if (uploadedImage.width < 1000 && uploadedImage.height < 1000) {
			return [uploadedImage.width, uploadedImage.height];
		} else {
			const width = 1000;
			const widthScale = width / uploadedImage.width;
			const height = uploadedImage.height * widthScale;
			return [width, height];
		}
	}

	drawImageOnCanvas(element, canvas) {
		//if there is a canvas and the element has a file, draw it on the canvas, then dispatch an event.
		const drawingSuccess = new CustomEvent("drawingSuccess", { detail: null });
		const drawingFailure = new CustomEvent("drawingFailure", { detail: null });
		const beginDraw = new CustomEvent("beginDraw", { detail: null });
		const endDraw = new CustomEvent("endDraw", { detail: null });
		if (canvas && element && element.files && !!element.files[0]) {
			const fileReader = new FileReader();
			let uploadedImage = new Image();
			canvas.dispatchEvent(beginDraw);
			fileReader.readAsDataURL(element.files[0]);
			fileReader.addEventListener("load", e => { uploadedImage.src = e.target.result });
			uploadedImage.onload = () => {
				const [ width, height ] = this._calculateCanvasDimensions(uploadedImage);
				console.warn("yohten", width, height);
				canvas.width = width;
				canvas.height = height;
				return new Promise(resolve => {
					const canvasContext = canvas.getContext('2d');
					resolve(canvasContext.drawImage(uploadedImage, 0, 0, width, height));
				}).then(() => {
					canvas.dispatchEvent(drawingSuccess);
					canvas.dispatchEvent(endDraw);
				}).catch(() => {
					canvas.dispatchEvent(drawingFailure);
					canvas.dispatchEvent(endDraw);
				})
			};
		}
	};

	readQRCode(canvas) {
		const codeReadFailure = new CustomEvent("codeReadFailure", { detail: null });
		const canvasContext = canvas.getContext('2d');
		const imageData = canvasContext.getImageData(0, 0, canvas.width, canvas.height);
		if (imageData && imageData.data) {
			const code = window.jsQR(imageData.data, canvas.width, canvas.height);
			if (code && code.data) {
				//custom events are read only, so this event has to be declared here.
				const codeReadSuccess = new CustomEvent("codeReadSuccess", { detail: code.data });
				canvas.dispatchEvent(codeReadSuccess);
			} else {
				canvas.dispatchEvent(codeReadFailure);
			}
		} else {
			canvas.dispatchEvent(codeReadFailure);
		}
	};

	saveCodeToLocalStorage(code, localStorageKey) {
		const lsSaveSuccess = new CustomEvent("lsSaveSuccess", { detail: null });
		const lsSaveFailure = new CustomEvent("lsSaveFailure", { detail: null });
		try {
			let ls = localStorage.getItem(localStorageKey);
			try {
				let parsedLs = JSON.parse(ls);
				if(parsedLs) {
					parsedLs.push(code);
					localStorage.setItem(localStorageKey, JSON.stringify(parsedLs));
					window.dispatchEvent(lsSaveSuccess);
				} else {
					localStorage.setItem(localStorageKey, JSON.stringify([code]));
					window.dispatchEvent(lsSaveSuccess);
				}
			} catch {
				window.dispatchEvent(lsSaveFailure);
			}
		} catch {
			window.dispatchEvent(lsSaveFailure);
		}
	};

	_removeClassFromElementAfterTimeout(element, className, timeout) {
		setTimeout(() => {
			element.classList.remove(className)
		}, timeout);
	}

	handleError(element, type) {
		if (element) {
			element.classList.add("active");
			switch (type) {
				case "draw":
					element.innerText = "Unfortunately drawing the image failed. Please ensure you are uploaded a valid image.";
					break;
				case "read":
					element.innerText = "The image does not appear to contain a QR code. Please try again.";
					break;
				case "save":
					element.innerText = "While the image contains a QR code, saving the code failed. Please try again.";
					break;
				default:
				//do nothing there is no default case here.
			}
			this._removeClassFromElementAfterTimeout(element, "active", 3000);
		}
	};

	handleSuccess(element) {
		if(element) {
			element.classList.add("active");
			element.innerText = "Successfully saved coffee data!";
			this._removeClassFromElementAfterTimeout(element, "active", 3000);
		}
	}

	displayLoadingSpinner(backdrop, spinner) {
		if(backdrop && spinner) {
			backdrop.classList.add("active");
			spinner.classList.add("active");
		}
	}

	hideLoadingSpinner(backdrop, spinner) {
		if(backdrop && spinner) {
			backdrop.classList.remove("active");
			spinner.classList.remove("active");
		}
	}
}

export default App

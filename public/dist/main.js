import App from "./app.js"

const app = new App();

const qrInput = document.getElementById("qrInput");
const canvas = document.getElementById("drawingBoard");
const successMessage = document.getElementById("successMessage");
const failureMessage = document.getElementById("failureMessage");
const backdrop = document.getElementById("backdrop");
const spinner = document.getElementById("spinner");
const lsKey = "cropsterLSKey";

//main workflow logic when picture uploaded, draw. when draw succeeds, read. when read succeeds, save.
app.attachEventListener(qrInput, "change", () => app.drawImageOnCanvas(qrInput, canvas));
app.attachEventListener(canvas, "drawingSuccess", () => app.readQRCode(canvas));
app.attachEventListener(canvas, "codeReadSuccess", e => app.saveCodeToLocalStorage(e.detail, lsKey));
app.attachEventListener(window, "lsSaveSuccess", () => app.handleSuccess(successMessage));

//error handling logic. if draw fails, or code read fails, or save fails, handle and display failure message.
app.attachEventListener(canvas, "drawingFailure", () => app.handleError(failureMessage, "draw"));
app.attachEventListener(canvas, "codeReadFailure", () => app.handleError(failureMessage, "read"));
app.attachEventListener(window, "lsSaveFailure", () => app.handleError(failureMessage, "save"));

//user experience logic, when draw begins, show loading spinner. when finished, hide it.
app.attachEventListener(canvas, "beginDraw", () => app.displayLoadingSpinner(backdrop, spinner));
app.attachEventListener(canvas, "endDraw", () => app.hideLoadingSpinner(backdrop, spinner));